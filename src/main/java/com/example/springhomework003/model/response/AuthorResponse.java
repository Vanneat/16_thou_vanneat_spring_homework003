package com.example.springhomework003.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorResponse<T> {
    private Timestamp timestamp;
    private String message;
    private HttpStatus httpStatus;
    private T payload;


}
