package com.example.springhomework003.service.serviceImp;

import com.example.springhomework003.controller.exception.AuthorNotFoundException;
import com.example.springhomework003.model.entity.Author;
import com.example.springhomework003.model.request.AuthorRequest;
import com.example.springhomework003.repository.AuthorRepository;
import com.example.springhomework003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorSeviceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorSeviceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
            return authorRepository.findAllAuthors();

    }

    @Override
    public Author getAuthorById(Integer authorId) {
        Author author = authorRepository.getAuthorById(authorId);
        if (author == null){
            throw new AuthorNotFoundException("Author ID " + authorId + " Not Found");
        }
        else
            return authorRepository.getAuthorById(authorId);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        if(authorRepository.deleteAuthorsById(authorId)==false){
            throw new AuthorNotFoundException("Author ID " + authorId + " Not Found");
        }
        else {
            return authorRepository.deleteAuthorsById(authorId);
        }

    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {
          Integer authorId = authorRepository.saveAuthor(authorRequest);
          return authorId;
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        if(authorRepository.updateAuthor(authorRequest, authorId)==null){
            throw new AuthorNotFoundException("Author ID " + authorId + " Not Found");
        }
        else {

            Integer authorIdStoreUpdate = authorRepository.updateAuthor(authorRequest,authorId);
            return authorIdStoreUpdate;
        }

    }

}
