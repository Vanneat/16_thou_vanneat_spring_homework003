package com.example.springhomework003.repository;

import com.example.springhomework003.model.entity.Author;
import com.example.springhomework003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface AuthorRepository {
    @Select("SELECT * FROM authors")
    List<Author> findAllAuthors();

    @Select("SELECT * FROM authors WHERE author_id = #{authorId}")
    Author getAuthorById(Integer authorId);
    @Delete("DELETE FROM authors WHERE author_id = #{authorId}")
    boolean deleteAuthorsById(Integer authorId);


    @Select("INSERT INTO authors (author_name, author_gender) VALUES(#{request.author_name},#{request.author_gender}) RETURNING author_id")
    Integer saveAuthor(@Param("request")AuthorRequest authorRequest);

    @Select("UPDATE authors " +
            "SET author_name = #{request.author_name}, " +
            "author_gender = #{request.author_gender} " +
            "WHERE author_id = #{authorId} " +
            "RETURNING author_id"
    )
    Integer updateAuthor(@Param("request") AuthorRequest authorRequest, Integer authorId);


}
