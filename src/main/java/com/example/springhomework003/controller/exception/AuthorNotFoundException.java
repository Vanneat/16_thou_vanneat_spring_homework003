package com.example.springhomework003.controller.exception;

public class AuthorNotFoundException extends RuntimeException{
    public AuthorNotFoundException(String message){
           super(message);
    }

}
