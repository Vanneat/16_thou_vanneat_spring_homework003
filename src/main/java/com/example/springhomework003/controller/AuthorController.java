package com.example.springhomework003.controller;

import com.example.springhomework003.model.entity.Author;
import com.example.springhomework003.model.request.AuthorRequest;
import com.example.springhomework003.model.response.AuthorResponse;
import com.example.springhomework003.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/author")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    @GetMapping("/all")

    public ResponseEntity<AuthorResponse<List<Author>>> getAllProduct(){

        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .message("Fetch successful")
                .payload(authorService.getAllAuthor())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<AuthorResponse<Author>>  getAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                .message("Fetch by ID successful")
                .payload(authorService.getAuthorById(authorId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<String> response = null;
        if (authorService.deleteAuthorById(authorId)==true){
            response = AuthorResponse.<String>builder()
                    .message("Delete By Id successful")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }


    @PostMapping
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){

        AuthorResponse<Author> response=null;
        Integer storeAuthorId = authorService.addNewAuthor(authorRequest);

        if(authorService.getAuthorById(storeAuthorId) != null){
            response = AuthorResponse.<Author>builder()
                    .message("Add Product success")
                    .httpStatus(HttpStatus.OK)
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<AuthorResponse<Author>> updateAuthor(
            @RequestBody AuthorRequest authorRequest, @PathVariable("id") Integer authorId
    ){
        AuthorResponse<Author> response = null;

        if (authorService.getAuthorById(authorId) != null){
            Integer authorIdStoreUpdate = authorService.updateAuthor(authorRequest,authorId);
            response = AuthorResponse.<Author>builder()
                    .message("Update success")
                    .payload(authorService.getAuthorById(authorIdStoreUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

}
